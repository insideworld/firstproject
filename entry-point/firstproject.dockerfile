FROM openjdk:8-alpine
COPY /target/firstproject-spring-boot.jar /firstproject/
ENTRYPOINT ["java","-jar","/firstproject/firstproject-spring-boot.jar"]
/*
 * Copyright
 */

package com.firstproject.entrypoint;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Spring boot config class.
 *
 * @since 0.0.1
 */
@SpringBootApplication(scanBasePackages = "com.firstproject")
@EnableJpaRepositories(basePackages = "com.firstproject.backend.database")
@EntityScan(basePackages = "com.firstproject.backend.database")
public class SpringBootConfig {
}

# FirstProject

Template of the first project for EDU students

In docs folder you can find training materials.

Useful links:

| What | Where |
| ------ | ------ |
| Maven | https://proselyte.net/tutorials/maven/ |
| Spring Boot easy tutorial | https://spring.io/guides/gs/spring-boot/ |
| Angular 4 video guide | https://disk.yandex.ru/d/cUKRT9ahkBZdeg |
| Markdown cheatsheet | https://paulradzkov.com/2014/markdown_cheatsheet/ |

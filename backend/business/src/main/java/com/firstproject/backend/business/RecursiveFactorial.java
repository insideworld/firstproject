/*
 * Copyright
 */

package com.firstproject.backend.business;

import com.firstproject.backend.database.FactorialCrud;
import com.firstproject.backend.database.FactorialResultsCrud;
import org.springframework.stereotype.Component;

/**
 * Implementation of recursive factorial.
 *
 * @since 0.0.1
 */
@Component
public class RecursiveFactorial extends AbstractFactorial {

    /**
     * Constructor.
     *
     * @param factorial Crud for factorial.
     * @param results Crud for results.
     */
    public RecursiveFactorial(final FactorialCrud factorial, final FactorialResultsCrud results) {
        super(factorial, results);
    }

    @Override
    public final boolean canApply(final int value) {
        final int more = 5;
        return value >= more;
    }

    @Override
    protected final int performCalculation(final int value) {
        final int result;
        if (value == 0) {
            result = 1;
        } else {
            result = value * this.performCalculation(value - 1);
        }
        return result;
    }

    @Override
    protected final String getAlgorithmName() {
        return "recursive";
    }

}
